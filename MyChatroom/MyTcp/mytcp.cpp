
#include "mytcp.h"

#ifdef _WIN32
int MyBasicTcp::FreeWinSockDll()
{
	WSACleanup();
	return 0;
}

int MyBasicTcp::InitWinSockDll()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
	wVersionRequested = MAKEWORD(2, 2);

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0) {
		/* Tell the user that we could not find a usable */
		/* Winsock DLL.                                  */
		ErrMsg(">>(error): WSAStartup failed with error: %d\n", err);
		return 1;
	}

	/* Confirm that the WinSock DLL supports 2.2.*/
	/* Note that if the DLL supports versions greater    */
	/* than 2.2 in addition to 2.2, it will still return */
	/* 2.2 in wVersion since that is the version we      */
	/* requested.                                        */

	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		ErrMsg(">>(error): Could not find a usable version of Winsock.dll\n");
		WSACleanup();
		return 1;
	}
	else
		InfoMsg("(Info): The Winsock 2.2 dll was found okay\n");


	/* The Winsock DLL is acceptable. Proceed to use it. */

	/* Add network programming using Winsock here */

	/* then call WSACleanup when done using the Winsock dll */

	//WSACleanup();
	return 0;
}
#endif

void MyBasicTcp::Close()
{
	MyCloseSocket(mSockFd);
#ifdef _WIN32
	FreeWinSockDll();
#endif

	mSockFdList.clear();
	FD_ZERO(&mReadFds);
	mSockFd = 0;
	mFdMax = 0;
}

MyBasicTcp::MyBasicTcp()
{
	/* initialize variables */
	mSockFdList.clear();
	FD_ZERO(&mReadFds);
	mSockFd = 0;
	mFdMax = 0;
}

MyBasicTcp::~MyBasicTcp()
{
	Close();
}

/* get IPv4 or IPv6 from sockaddr */
void* MyBasicTcp::GetInAddr(struct sockaddr* sa)
{
	if (sa->sa_family == AF_INET)
	{
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

#ifdef __linux__
void SigchldHandler(int s)
{
	while (waitpid(-1, NULL, WNOHANG) > 0);
}

int KillZombieProcess()
{
	struct sigaction sa; // for linux kill zombie process

	/* clear all linux zombie process*/
	sa.sa_handler = SigchldHandler; // kill all zombie process
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if (sigaction(SIGCHLD, &sa, NULL) == -1)
	{
		return 1;
	}
	return 0;
}
#endif
//------------------------------------------------------------------------------------------
int MyBasicTcp::Send(const char* buf, int len)
{
	return send(mSockFd, buf, len, 0);
}

int MyBasicTcp::Receive(char* buf, int len)
{
	return recv(mSockFd, buf, len, 0);
}

int MyBasicTcp::SendAll(int dstScokFd, char* buf, int* len)
{
	int total = 0; // total bytes which already sent
	int byteLeft = *len; // how many bytes still need to send
	int rcv;

	while (total < *len)
	{
		if(dstScokFd == NULL)
			rcv = send(mSockFd, buf + total, byteLeft, 0);
		else
			rcv = send(dstScokFd, buf + total, byteLeft, 0);
		if (rcv == -1)
			break;
		total += rcv;
		byteLeft -= rcv;
	}

	*len = total;

	if (rcv != -1 && rcv != 0)
		rcv = total;

	return rcv;
}

int MyBasicTcp::ReceiveAll(int dstScokFd, char* buf, int* len)
{
	int total = 0; // total bytes which already sent
	int byteLeft = *len; // how many bytes still need to send
	int rcv;

	while (total < *len)
	{
		if (dstScokFd == NULL)
			rcv = recv(mSockFd, buf + total, byteLeft, 0);
		else
			rcv = recv(dstScokFd, buf + total, byteLeft, 0);
		if (rcv == -1)
			break;
		if (rcv == 0) // maybe server shutdown
			break;
		total += rcv;
		byteLeft -= rcv;
	}

	*len = total;

	if (rcv != -1 && rcv != 0)
		rcv = total;

	return rcv;
}
//------------------------------------------------------------------------------------------
void MyBasicTcp::CopyMainFdList_NoHost(std::vector<SocketFdInfo>* copyFdList)
{
	copyFdList->clear();

	for (int i = 0; i < (int)mSockFdList.size(); i++)
	{
		if (mSockFdList[i].sockFd == mSockFd)
			continue;
		copyFdList->push_back(mSockFdList[i]);
	}

}

void MyBasicTcp::UpdateMainFdListInfo(SocketFdInfo updateFd)
{
	for (int i = 0; i < (int)mSockFdList.size(); i++)
	{
		if (mSockFdList[i].sockFd == updateFd.sockFd)
		{
			memcpy(mSockFdList[i].name, updateFd.name, strlen(updateFd.name));
			break;
		}
	}
}

void MyBasicTcp::AddToMainFdList(int newFd)
{
	SocketFdInfo sfdInfo;
	
	memset(&sfdInfo, 0, sizeof(sfdInfo));
	sfdInfo.sockFd = newFd;
	mSockFdList.push_back(sfdInfo); // add this new connection to master
	if (newFd > mFdMax)
		mFdMax = newFd; // update mFdMax
}

void MyBasicTcp::EraseFromMainFdList(int eraseSockFd)
{
	for (int i = 0; i < (int)mSockFdList.size(); i++)
	{
		if (mSockFdList[i].sockFd == eraseSockFd)
		{
			mSockFdList.erase(mSockFdList.begin() + i);
		}
	}
}

void MyBasicTcp::ClearMainFdList()
{
	mSockFdList.clear();
	mFdMax = 0;
}
//------------------------------------------------------------------------------------------
int MyBasicTcp::SetupHost(const char* port)
{
	/* variable */
	int status;
	char yes = 1; // for setsockopt() to configure SO_REUSEADDR
	struct addrinfo hints;
	struct addrinfo* hostInfo;
	struct addrinfo* p;

#ifdef _WIN32
	status = InitWinSockDll();
	if (status != 0)
	{
		ErrWMsg(L">>(error): CheckWinSockDll() failed\n");
		return 1;
	}
#endif

	/* get hostInfo */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	status = getaddrinfo(NULL, port, &hints, &hostInfo);
	if (status != 0)
	{
		ErrMsg(">>(error): SetupHost::getaddrinfo - \"%s\"\n", gai_strerror(status));
		return 1;
	}

	/* create socket() at first could be bind in hostInfo */
	for (p = hostInfo; p != NULL; p = p->ai_next)
	{
		mSockFd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (mSockFd == -1)
		{
			ErrMsg(">>(error): SetupHost::socket \"%s\" (errno: %d), try next\n", strerror(errno), errno);
			continue;
		}

		/* skip its error when happened */
		setsockopt(mSockFd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));

		/* create bind() */
		if (bind(mSockFd, p->ai_addr, p->ai_addrlen) == -1)
		{
			ErrMsg(">>(error): SetupHost::bind \"%s\" (errno: %d), try next\n", strerror(errno), errno);
			MyCloseSocket(mSockFd);
			continue;
		}

		break; // scoket created and bind successfully.
	}

	/* if p is NULL, it means bind() is failed */
	if (p == NULL)
	{
		ErrMsg(">>(error): no any socket can be bind\n");
		return 2;
	}

	freeaddrinfo(hostInfo); // all done with this

#ifdef __linux__
	if (KillZombieProcess() != 0)
	{
		perror("server: sigaction fail");
		return 1;
	}
#endif

	/* create listen() */
	if (listen(mSockFd, BACKOG) == -1)
	{
		ErrMsg(">>(error): SetupHost::listen \"%s\" (errno: %d)\n", strerror(errno), errno);
		return 1;
	}

	AddToMainFdList(mSockFd); // add host socket file descriptor into master set

	return 0;
}

int MyBasicTcp::SetupClient(const char* ip, const char* port)
{
	/* variable */
	struct addrinfo hints;
	struct addrinfo* hostInfo;
	struct addrinfo* p; // point for for-loop
	int status;
	char hostIP[INET6_ADDRSTRLEN];
	struct timeval tv;


#ifdef _WIN32
	status = InitWinSockDll();
	if (status != 0)
	{
		ErrWMsg(L">>(error): CheckWinSockDll() failed\n");
		return 1;
	}
#endif

	DbgMsg("SetupClient get ip=\"%s\", port=\"%s\"\n", ip, port);

	/* get hostInfo */
	DbgMsg("  do getaddrinfo()\n");
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC; // IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM;
	status = getaddrinfo(ip, port, &hints, &hostInfo);
	if (status != 0)
	{
		ErrWMsg(L">>(error): SetupClient::getaddrinfo \"%s\" (%d)\n", gai_strerror(status), status);
		return 1;
	}

	/* create socket() at first could be bind in hostInfo */
	DbgMsg("  do create socket() at first could be bind in hostInfo\n");
	for (p = hostInfo; p != NULL; p = p->ai_next)
	{
		mSockFd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		DbgMsg("    [in for loop]: socket is %d\n", mSockFd);
		if (mSockFd == -1)
		{
			//perror("server: socket fail and try next");
			ErrMsg(">>(error): SetupClient::socket \"%s\" (errno: %d), try next\n", strerror(errno), errno);
			continue;
		}


		/* set timeout */
		DbgMsg("    [in for loop]: set timeout for connect()\n");
		tv.tv_sec = 3;
		tv.tv_usec = 0;
		setsockopt(mSockFd, SOL_SOCKET, SO_SNDTIMEO, (char*)&tv, sizeof(tv));

		/* create connect() */
		DbgMsg("    [in for loop]: do connect()\n");
		if (connect(mSockFd, p->ai_addr, p->ai_addrlen) == -1)
		{
			if(errno == EINPROGRESS)
				ErrMsg(">>(error): SetupClient::connect timeout, try next\n");
			else
				ErrMsg(">>(error): SetupClient::connect \"%s\" (errno: %d), try next\n", strerror(errno), errno);
			MyCloseSocket(mSockFd);
			continue;
		}
		DbgMsg("    [in for loop]: bind socket %d success\n", socket);
		break; // create socket pass and bind pass
	}

	if (p == NULL)
	{
		ErrMsg(">>(error): client failed to connect\n");
		return 2;
	}

	/* list host IP you connected */
	inet_ntop(p->ai_family, GetInAddr((struct sockaddr*)(p->ai_addr)), hostIP, sizeof(hostIP));
	InfoMsg("(Info): You connect to %s and join now!!\n", hostIP);

	freeaddrinfo(hostInfo); // free hostInfo here (not be used below)

	AddToMainFdList(mSockFd); // add host socket file descriptor into master set


	return 0;
}

int MyBasicTcp::Accepted()
{
	/* handle new connections */

	struct sockaddr_storage remoteAddr;		// client address
	socklen_t addrLen;
	int newFd;			// new socket descriptor with accept()
	char remoteIP[INET6_ADDRSTRLEN];
	char msg[MAXMSGLENGTH] = { 0 };

	addrLen = sizeof(remoteAddr);
	newFd = accept(mSockFd, (struct sockaddr*)&remoteAddr, &addrLen);
	if (newFd == -1)
	{
		ErrMsg(">>(error): Accepted::accept \"%s\" (errno: %d)\n", strerror(errno), errno);
		return 1;
	}
	else
	{
		AddToMainFdList(newFd); // add newFd to master list
		memset(remoteIP, 0, sizeof(INET6_ADDRSTRLEN));
		inet_ntop(remoteAddr.ss_family, GetInAddr((struct sockaddr*)&remoteAddr), remoteIP, sizeof(remoteIP));
		InfoMsg("(Info): new connection from %s on socket %d\n", remoteIP, newFd);

		/* send hello word message to client */
		sprintf(msg, "Welcome to join! Your IP is %s", remoteIP);
		MsgWhenServerAccepted(newFd, msg, strlen(msg));
		//sprintf(msgPack.name, "Server");
		//sprintf(msgPack.msg, "Welcome to join! Your IP is %s", remoteIP);
		//msgPack.nameLen = strlen(msgPack.name);
		//msgPack.msgLen = strlen(msgPack.msg);
		//SendPack_Msg(newFd, &msgPack);

	}
	return 0;
}

int MyBasicTcp::SelectWaitingfor(struct timeval* timeout)
{
	/* copy master set*/
	int status;
	FD_ZERO(&mReadFds);
	for (int i = 0; i < (int)mSockFdList.size(); i++)
	{
		FD_SET(mSockFdList[i].sockFd, &mReadFds);
	}

	/* using select() to monitor sockets without timeout */
	status = select(mFdMax + 1, &mReadFds, NULL, NULL, timeout);
	if (status == -1)
		ErrMsg(">>(error): SelectWaitingfor::select\n");
	return status;
}

int MyBasicTcp::GetActivitySockfdHost(std::vector<SocketFdInfo>* activeList)
{
	int resultSockFd = -1; // -1 is no activity socket

	/* find the data in mReadFds which updated in SelectWaitingfor() */
	for (int list = 0; list < (int)mSockFdList.size(); list++)
	{
		int i = mSockFdList[list].sockFd;
		if (FD_ISSET(i, &mReadFds) == true) // find one!!
		{
			resultSockFd = 0;
			if (i == mSockFd)
			{
				Accepted(); // handle new connections
			}
			else
			{
				activeList->push_back(mSockFdList[list]);
			}
		}
	}

	return resultSockFd;
}

int MyBasicTcp::GetActivitySockfdClient(std::vector<SocketFdInfo>* activeList)
{
	int resultSockFd = -1; // -1 is no activity socket

	/* find the data in mReadFds which updated in SelectWaitingfor() */
	for (int list = 0; list < (int)mSockFdList.size(); list++)
	{
		int i = mSockFdList[list].sockFd;
		if (FD_ISSET(i, &mReadFds) == true) // find one!!
		{
			resultSockFd = 0;
			activeList->push_back(mSockFdList[list]);
		}
	}

	return resultSockFd;
}
//------------------------------------------------------------------------------------------
bool MyBasicTcp::IsPackTypeInDefine(unsigned int type)
{
	bool result = false;

	switch (type)
	{
		case BROADCAST_USRJOIN:
		case COMMONMESSAGEPACK:
			result = true;
			break;
		default:
			result = false;
			break;
	}
	return result;
}

unsigned int MyBasicTcp::GetPackType(int dstSockFd)
{
	int rcv;
	int bufferLen = 0;
	unsigned char* buffer;
	unsigned int type = 0;

	DbgMsg("GetPackType >>>:\n");

	bufferLen = sizeof(unsigned int);
	buffer = new unsigned char[bufferLen];
	rcv = ReceiveAll(dstSockFd, (char*)buffer, &bufferLen); // receive pack header
	DbgMsg("  receive bytes is %d (expect %d)\n", rcv, bufferLen);
	if (rcv <= 0)
	{
		delete buffer;
		ErrMsg(">>(error): GetPackType::ReceiveAll fail (value = %d)\n", rcv);
		return rcv;
	}

	unpack(buffer, "L", &type);
	DbgMsg("  unpack: type is 0x%X\n", type);
	delete buffer;
	
	return type;
}

int MyBasicTcp::ReceivePack(int dstSockFd, unsigned int type, void* buf, int* len, bool asRawData)
{
	int rcv = 0;
	unsigned char* pBuffer = NULL;
	int bufferLen = 0;
	unsigned int dataBytesInPack = 0;

	DbgMsg("ReceivePack >>>:\n");

	*len = 0;

	bufferLen = sizeof(unsigned int);
	pBuffer = new unsigned char[bufferLen];
	rcv = ReceiveAll(dstSockFd, (char*)pBuffer, &bufferLen); // receive pack total length
	DbgMsg("  receive bytes is %d (expect %d)\n", rcv, bufferLen);
	if (rcv <= 0)
	{
		delete pBuffer;
		ErrMsg(">>(error): ReceivePack::ReceiveAll get total bytes fail (value = %d)\n", rcv);
		return rcv;
	}

	unpack(pBuffer, "L", &dataBytesInPack); // get data package bytes
	DbgMsg("  unpack: dataBytesInPack is %d (0x%X)\n", dataBytesInPack, dataBytesInPack);
	delete pBuffer;

	/* start to receive remaining data */
	bufferLen = dataBytesInPack;
	pBuffer = new unsigned char[bufferLen];
	rcv = ReceiveAll(dstSockFd, (char*)pBuffer, &bufferLen); // receive data package
	DbgMsg("  receive data bytes is %d (expect %d)\n", rcv, bufferLen);
	if (rcv <= 0)
	{
		delete pBuffer;
		ErrMsg(">>(error): ReceivePack::ReceiveAll get data bytes fail (value = %d)\n", rcv);
		return rcv;
	}

#ifdef __DEBUG__
	DbgMsg("  receive raw data except the size of 'header' and 'dataBytesInPack'\n");
	DbgMsg("    raw data : ");
	for (int i = 0; i < bufferLen; i++)
		DbgMsg("%x, ", pBuffer[i]);
	DbgMsg("\n");
#endif

	if (asRawData == true)
	{
		/* receive as raw data with type */
		memset(buf, 0, *len); // clear input buffer

		int finalLen = 0;
		unsigned char* tempBuff = NULL;
		int tempBuffLen = 0;

		/* pack 'type' and copy to buf */
		tempBuffLen = sizeof(unsigned int); // size of 'type'
		tempBuff = new unsigned char[tempBuffLen]; // for 'type' to pack
		pack(tempBuff, "L", type); // pack 'type'
		memcpy((unsigned char*)buf + finalLen, tempBuff, tempBuffLen);
		finalLen += tempBuffLen;
		delete tempBuff;

		/* pack 'dataBytesInPack' and copy to buf */
		tempBuffLen = sizeof(unsigned int); // size of 'dataBytesInPack'
		tempBuff = new unsigned char[tempBuffLen]; // for 'data bytes'
		pack(tempBuff, "L", dataBytesInPack); // pack 'data bytes'
		memcpy((unsigned char*)buf + finalLen, tempBuff, tempBuffLen);
		finalLen += tempBuffLen;
		delete tempBuff;

		/* copy 'data' to buf  */
		memcpy((unsigned char*)buf + finalLen, pBuffer, bufferLen);
		finalLen += bufferLen;

		*len = finalLen;
		rcv = finalLen;
	}
	else
	{
		/* unpack the data */
		if (type == BROADCAST_USRJOIN)
		{
			UserJoinPackage* pUsrJoinPack = (UserJoinPackage*)buf;

			unpack(pBuffer, "Ls",
				&pUsrJoinPack->msgLen,
				pUsrJoinPack->msg);
			DbgMsg("    unpack UserJoinPackage:\n");
			DbgMsg("      msg=\"%s\" (expect len=%d, actual=%d)\n",
				pUsrJoinPack->msg, pUsrJoinPack->msgLen, strlen(pUsrJoinPack->msg));

			*len = bufferLen;
			rcv = bufferLen;
		}
		if (type == COMMONMESSAGEPACK)
		{
			MsgPackage* pMsgPack = (MsgPackage*)buf;

			unpack(pBuffer, "LLss",
				&pMsgPack->nameLen,
				&pMsgPack->msgLen,
				pMsgPack->name,
				pMsgPack->msg);
			DbgMsg("    unpack MsgPackage:\n");
			DbgMsg("      name=\"%s\" (expect len=%d, actual=%d)\n",
				pMsgPack->name, pMsgPack->nameLen, strlen(pMsgPack->name));
			DbgMsg("      msg=\"%s\" (expect len=%d, actual=%d)\n",
				pMsgPack->msg, pMsgPack->msgLen, strlen(pMsgPack->msg));

			*len = bufferLen;
			rcv = bufferLen;
		}

	}
	if (pBuffer != NULL)
		delete pBuffer;

	return rcv;
}

int MyBasicTcp::SendPack(int dstSockFd, unsigned int type, void* buf, int* len, bool asRawData)
{
	int sentLen = 0;
	unsigned int packDataBytes = 0;
	unsigned int dataLen = 0; // bytes in pack()
	unsigned char* packBuff = NULL;

	DbgMsg("SendPack >>>:\n");

	if (asRawData == true)
	{
#ifdef __DEBUG__
		DbgMsg("  send raw data (%d bytes)\n", *len);
		DbgMsg("    raw data : ");
		for (int i = 0; i < *len; i++)
			DbgMsg("%x, ", ((char*)buf)[i]);
		DbgMsg("\n");
#endif
		/* treat buf as raw data, and len is its size */
		sentLen = SendAll(dstSockFd, (char*)buf, len); // send dircetally
		return sentLen;
	}

	/* pack buf with type then send */
	if (type == COMMONMESSAGEPACK)
	{
		MsgPackage* pMsgPack = (MsgPackage*)buf;

		dataLen = sizeof(pMsgPack->nameLen) +
			sizeof(pMsgPack->msgLen) +
			pMsgPack->nameLen + 2 + // 2=> '1' for store nameLen length in pack(), another '1' is '\0'
			pMsgPack->msgLen + 2;   // 2=> '1' for store msgLen length in pack(), another '1' is '\0'
		pMsgPack->dataBytesInPack = dataLen;
		DbgMsg("  pack pMsgPack:\n");
		DbgMsg("    header = 0x%X\n", pMsgPack->header);
		DbgMsg("    dataBytesInPack = %d (size %d)\n", pMsgPack->dataBytesInPack, sizeof(pMsgPack->dataBytesInPack));
		DbgMsg("    nameLen = %d (size %d)\n", pMsgPack->nameLen, sizeof(pMsgPack->nameLen));
		DbgMsg("    msgLen = %d (size %d)\n", pMsgPack->msgLen, sizeof(pMsgPack->msgLen));
		DbgMsg("    name = %s\n", pMsgPack->name);
		DbgMsg("    msg = %s\n", pMsgPack->msg);

		dataLen += sizeof(pMsgPack->header) + sizeof(pMsgPack->dataBytesInPack); // total byte will be in pack()

		packBuff = new unsigned char[dataLen];
		memset(packBuff, 0, dataLen);

		packDataBytes = pack(packBuff, "LLLLss",
			pMsgPack->header,
			pMsgPack->dataBytesInPack,
			pMsgPack->nameLen,
			pMsgPack->msgLen,
			pMsgPack->name,
			pMsgPack->msg); // pack user name + message

#ifdef __DEBUG__
		DbgMsg("  after pack\n");
		DbgMsg("    packDataBytes=%d, dataLen=%d\n", packDataBytes, dataLen);
		DbgMsg("    raw data : ");
		for (int i = 0; i < packDataBytes; i++)
			DbgMsg("%x, ", packBuff[i]);
		DbgMsg("\n");
#endif

		sentLen = SendAll(dstSockFd, (char*)packBuff, (int*)&dataLen); // send
	}
	if (type == BROADCAST_USRJOIN)
	{
		UserJoinPackage* pUsrJoinPack = (UserJoinPackage*)buf;

		dataLen = sizeof(pUsrJoinPack->msgLen) +
			pUsrJoinPack->msgLen + 2;   // 2=> '1' for store msgLen length in pack(), another '1' is '\0'
		pUsrJoinPack->dataBytesInPack = dataLen;
		DbgMsg("  pack pUsrJoinPack:\n");
		DbgMsg("    header = 0x%X\n", pUsrJoinPack->header);
		DbgMsg("    dataBytesInPack = %d (size %d)\n", pUsrJoinPack->dataBytesInPack, sizeof(pUsrJoinPack->dataBytesInPack));
		DbgMsg("    msgLen = %d (size %d)\n", pUsrJoinPack->msgLen, sizeof(pUsrJoinPack->msgLen));
		DbgMsg("    msg = %s\n", pUsrJoinPack->msg);

		dataLen += sizeof(pUsrJoinPack->header) + sizeof(pUsrJoinPack->dataBytesInPack); // total byte will be in pack()

		packBuff = new unsigned char[dataLen];
		memset(packBuff, 0, dataLen);

		packDataBytes = pack(packBuff, "LLLs",
			pUsrJoinPack->header,
			pUsrJoinPack->dataBytesInPack,
			pUsrJoinPack->msgLen,
			pUsrJoinPack->msg); // pack user name + message

#ifdef __DEBUG__
		DbgMsg("  after pack\n");
		DbgMsg("    packDataBytes=%d, dataLen=%d\n", packDataBytes, dataLen);
		DbgMsg("    raw data : ");
		for (int i = 0; i < packDataBytes; i++)
			DbgMsg("%x, ", packBuff[i]);
		DbgMsg("\n");
#endif

		sentLen = SendAll(dstSockFd, (char*)packBuff, (int*)&dataLen); // send
	}
	if (packBuff != NULL)
		delete packBuff;

	return sentLen;
}
//------------------------------------------------------------------------------------------
int MyBasicTcp::MsgWhenServerAccepted(int newSockFd, char* buf, int len)
{
	UserJoinPackage usrJoinPack;
	int addrLen;

	memcpy(usrJoinPack.msg, buf, len);
	usrJoinPack.msgLen = strlen(usrJoinPack.msg);
	return SendPack(newSockFd, BROADCAST_USRJOIN, (void*)&usrJoinPack, &addrLen, false);	
}
//------------------------------------------------------------------------------------------
#if 0
int MyBasicTcp::SendPack_Msg(int dstScokFd, MsgPackage* msgPack)
{
	int sentLen = 0;
	unsigned int packResultSize = 0;
	unsigned char* packBuff = NULL;
	//unsigned char packBuff[MAXDATASIZE];

	DbgMsg("SendPack_Msg >>>:\n");

	packResultSize =
		sizeof(msgPack->dataBytesInPack) +
		sizeof(msgPack->nameLen) +
		sizeof(msgPack->msgLen) +
		msgPack->nameLen + 2 + // 2=> '1' for store nameLen length in pack(), another '1' is '\0'
		msgPack->msgLen + 2;   // 2=> '1' for store nameLen length in pack(), another '1' is '\0'
	msgPack->dataBytesInPack = packResultSize;
	DbgMsg("  msgPack:\n");
	DbgMsg("    dataBytesInPack = %d (size=%d)\n", msgPack->dataBytesInPack, sizeof(msgPack->dataBytesInPack));
	DbgMsg("    nameLen = %d (size=%d)\n", msgPack->nameLen, sizeof(msgPack->nameLen));
	DbgMsg("    msgLen = %d (size=%d)\n", msgPack->msgLen, sizeof(msgPack->msgLen));
	DbgMsg("    name = %s\n", msgPack->name);
	DbgMsg("    msg = %s\n", msgPack->msg);

	packBuff = new unsigned char[msgPack->dataBytesInPack];
	//packBuff = new unsigned char[MAXDATASIZE];
	memset(packBuff, 0, msgPack->dataBytesInPack);

	packResultSize = pack(packBuff, "LLLss",
		msgPack->dataBytesInPack,
		msgPack->nameLen,
		msgPack->msgLen,
		msgPack->name,
		msgPack->msg); // pack user name + message

	//pack(packBuff, "L", packResultSize); // pack packBuff length

	DbgMsg("  pack msgPack raw data:\n    ");
	for (int i = 0; i < packResultSize; i++)
		DbgMsg("%x, ", packBuff[i]);
	DbgMsg("\n");

	sentLen = SendAll(dstScokFd, (char*)packBuff, (int*)&msgPack->dataBytesInPack); // send

	delete packBuff;

	return sentLen;
}

int MyBasicTcp::ReceivePack_Msg(int dstScokFd, MsgPackage* msgPack)
{
	int rcv;
	unsigned char* buffer;
	int bufferLen = 0;
	unsigned int tempHeaderSize = sizeof(msgPack->dataBytesInPack) + sizeof(msgPack->nameLen) + sizeof(msgPack->msgLen);


	DbgMsg("ReceivePack_Msg >>>:\n");
	memset(msgPack, 0, sizeof(MsgPackage));


	bufferLen = sizeof(msgPack->dataBytesInPack);
	buffer = new unsigned char[bufferLen];
	rcv = ReceiveAll(dstScokFd, (char*)buffer, &bufferLen); // receive pack header to get data length
	DbgMsg("  receive msgPack:\n");
	DbgMsg("    header length = %d (expect %d)\n", rcv, bufferLen);
	if (rcv <= 0)
	{
		delete buffer;
		return rcv;
	}

	unpack(buffer, "L", &msgPack->dataBytesInPack);
	if (msgPack->dataBytesInPack == BROADCAST_USRJOIN)
	{
		DbgMsg("    unpack: package total bytes is %d (0x%X)\n", BROADCAST_USRJOIN, BROADCAST_USRJOIN);
		DbgMsg("      =>It is a broadcast message\n");
		return BROADCAST_USRJOIN;
	}
	DbgMsg("    unpack: package total bytes is %d (header=%d + data=%d)\n", msgPack->dataBytesInPack, rcv, msgPack->dataBytesInPack - rcv);
	if (msgPack->dataBytesInPack <= (tempHeaderSize + 2)) // size of 'unsigned int' and two '\0'
	{
		/* error handle */
		memset(msgPack, 0, sizeof(MsgPackage));
		if (buffer != NULL)
			delete buffer;
		ErrMsg(">>(error): cannot get message package length\n");
		return -1;
	}

	/* start to receive remaining data */
	if (buffer != NULL)
		delete buffer;
	bufferLen = msgPack->dataBytesInPack - sizeof(msgPack->dataBytesInPack);
	buffer = new unsigned char[bufferLen];
	rcv = ReceiveAll(dstScokFd, (char*)buffer, &bufferLen); // receive data package
	DbgMsg("    data length = %d (expect %d)\n", rcv, bufferLen);
	DbgMsg("      raw data: ");
	for (int i = 0; i < rcv; i++)
		DbgMsg("%x, ", buffer[i]);
	DbgMsg("\n");
	if (rcv <= 0)
	{
		/* error handle */
		memset(msgPack, 0, sizeof(MsgPackage));
		if (buffer != NULL)
			delete buffer;
		ErrMsg(">>(error): cannot get message package data\n");
		return rcv;
	}

	unpack(buffer, "LLss",
		&msgPack->nameLen,
		&msgPack->msgLen,
		&msgPack->name,
		&msgPack->msg);
	DbgMsg("    unpack msg:\n");
	DbgMsg("      name=\"%s\" (expect len=%d, actual=%d)\n",
		msgPack->name, msgPack->nameLen, strlen(msgPack->name));
	DbgMsg("      msg=\"%s\" (expect len=%d, actual=%d)\n",
		msgPack->msg, msgPack->msgLen, strlen(msgPack->msg));

	if (buffer != NULL)
		delete buffer;
	rcv = msgPack->dataBytesInPack;

	return rcv;
}

int MyBasicTcp::SendPack_RawMsg(int dstScokFd, unsigned char* buff, int* len)
{
	DbgMsg("SendPack_RawMsg >>>:\n");
	return send(dstScokFd, (char*)buff, *len, 0);
}

int MyBasicTcp::ReceivePack_RawMsg(int dstScokFd, unsigned char* buff, int* len)
{
	int rcv;
	MsgPackage msgPack;
	int bufferLen = 0;
	unsigned char* packLenBuff = NULL;
	unsigned char* packDataBuff = NULL;
	unsigned int tempHeaderSize = sizeof(msgPack.dataBytesInPack) + sizeof(msgPack.nameLen) + sizeof(msgPack.msgLen);

	DbgMsg("ReceivePack_RawMsg >>>:\n");
	*len = 0;

	bufferLen = sizeof(msgPack.dataBytesInPack);
	packLenBuff = new unsigned char[bufferLen];
	rcv = ReceiveAll(dstScokFd, (char*)packLenBuff, &bufferLen); // receive pack header to get data length
	DbgMsg("  receive raw msgPack length: expect=%d (actual=%d)\n", bufferLen, rcv);
	if (rcv <= 0)
	{
		delete packLenBuff;
		return rcv;
	}

	unpack(packLenBuff, "L", &msgPack.dataBytesInPack);
	if (msgPack.dataBytesInPack == BROADCAST_USRJOIN)
	{
		DbgMsg("  unpack: package total bytes is %d (0x%X)\n", BROADCAST_USRJOIN, BROADCAST_USRJOIN);
		DbgMsg("    => It is a broadcast message\n");
		return BROADCAST_USRJOIN;
	}
	DbgMsg("  unpack: package total bytes is %d (header=%d + data=%d)\n", msgPack.dataBytesInPack, rcv, msgPack.dataBytesInPack - rcv);
	if (msgPack.dataBytesInPack <= (tempHeaderSize + 2)) // size of 'unsigned int' and two '\0'
	{
		/* error handle */
		if (packLenBuff != NULL)
			delete packLenBuff;
		ErrMsg(">>(error): cannot get raw message package length\n");
		return -1;
	}

	/* start to receive remaining data */
	bufferLen = msgPack.dataBytesInPack - sizeof(msgPack.dataBytesInPack);
	packDataBuff = new unsigned char[bufferLen];
	rcv = ReceiveAll(dstScokFd, (char*)packDataBuff, &bufferLen); // receive data package
	DbgMsg("  receive raw data length: expect=%d (actual=%d)\n", bufferLen, rcv);
	if (rcv <= 0)
	{
		delete packDataBuff;
		return rcv;
	}

	//*buff = new unsigned char[msgPack.dataBytesInPack];
	memcpy(buff, packLenBuff, sizeof(msgPack.dataBytesInPack));
	memcpy(buff + sizeof(msgPack.dataBytesInPack), packDataBuff, msgPack.dataBytesInPack - sizeof(msgPack.dataBytesInPack));
	*len = msgPack.dataBytesInPack;
	delete packLenBuff;
	delete packDataBuff;
	rcv = msgPack.dataBytesInPack;

	return rcv;
}

int MyBasicTcp::SendBroadcast_UserJoin(int dstSockFd, UserJoinPackage* usrJoinPack)
{
	int nBytes;
	unsigned char buf[MAXDATASIZE];

	nBytes = sizeof(usrJoinPack->header) +
		sizeof(usrJoinPack->dataBytesInPack) +
		sizeof(usrJoinPack->msgLen) +
		usrJoinPack->msgLen + 2;
	usrJoinPack->dataBytesInPack = nBytes;

	nBytes = pack(buf, "LLLs",
		usrJoinPack->header,
		usrJoinPack->dataBytesInPack,
		usrJoinPack->msgLen,
		usrJoinPack->msg);
	SendAll(dstSockFd, (char*)buf, &nBytes);

	return 0;
}

int MyBasicTcp::ReceiveBroadcast_UserJoin(int dstSockFd, UserJoinPackage* usrJoinPack)
{
	return 0;
}
#endif
//------------------------------------------------------------------------------------------