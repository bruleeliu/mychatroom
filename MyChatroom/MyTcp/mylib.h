#pragma once
#ifndef __MY_LIBRARY_H__
#define __MY_LIBRARY_H__


#include "commonheader.h"

//#define DEBUG
#ifdef DEBUG
#include <limits.h>
#include <float.h>
#include <assert.h>
#endif

#ifdef _DEBUG
#	define __DEBUG__
#endif

#if (__cplusplus < 199901L)
//#    error "Please use a newer compiler that support __VA_ARGS__."
//#    error "Please use a newer compiler that support args."
#ifdef __DEBUG__
#    define DbgMsg(format, ...)       fprintf(stderr, format, ## __VA_ARGS__)
#else
#    define DbgMsg(format, ...)
#endif
#    define ErrMsg(format, ...)       fprintf(stderr, format, ## __VA_ARGS__)
#    define ErrWMsg(format, ...)       fwprintf(stderr, format, ## __VA_ARGS__)
#    define InfoMsg(format, ...)       fprintf(stdout, format, ## __VA_ARGS__)
#else
#ifdef __DEBUG__
#    define DbgMsg(format, args...)       fprintf(stderr, format, ## args)
#else
#    define DbgMsg(format, args...)
#endif
#    define ErrMsg(format, args...)       fprintf(stderr, format, ## args)
#    define InfoMsg(format, args...)       fprintf(stdout, format, ## args)
#endif



#define pack754_16(f) (pack754((f), 16, 5))
#define pack754_32(f) (pack754((f), 32, 8))
#define pack754_64(f) (pack754((f), 64, 11))
#define unpack754_16(i) (unpack754((i), 16, 5))
#define unpack754_32(i) (unpack754((i), 32, 8))
#define unpack754_64(i) (unpack754((i), 64, 11))

uint64_t pack754(long double f, unsigned bits, unsigned expbits);
long double unpack754(uint64_t i, unsigned bits, unsigned expbits);

/*
** pack() -- store data dictated by the format string in the buffer
**
**   bits |signed   unsigned   float   string  | example
**   -----+------------------------------------+---------------
**      8 |   c        C                       | "char", "signed char", "unsigned char"
**     16 |   h        H         -             | "short", "unsigned short", "short int", "unsigned short int"
**     32 |   l        L         f             | "int", "unsigned int", "float" 
**     64 |   q        Q         d             | "double", "long", "unsigned long int", "long long int", "unsigned long long int", "long long", "long int"
**    128 |                      g(x)          | "long double"
**      - |                               s    |
**
**  (16-bit unsigned length is automatically prepended to strings)
*/
unsigned int pack(unsigned char* buf, const char* format, ...);
/*
** unpack() -- unpack data dictated by the format string into the buffer
**
**   bits |signed   unsigned   float   string  | example
**   -----+------------------------------------+---------------
**      8 |   c        C                       | "char", "signed char", "unsigned char"
**     16 |   h        H         -             | "short", "unsigned short", "short int", "unsigned short int"
**     32 |   l        L         f             | "int", "unsigned int", "float"
**     64 |   q        Q         d             | "double", "long", "unsigned long int", "long long int", "unsigned long long int", "long long", "long int"
**    128 |                      g(x)          | "long double"
**      - |                               s    |
**
**  (string is extracted based on its stored length, but 's' can be
**  prepended with a max length)
*/
void unpack(unsigned char* buf, const char* format, ...);

void packi16(unsigned char* buf, unsigned short int i);
short int unpacki16(unsigned char* buf);
unsigned short int unpacku16(unsigned char* buf);

void packi32(unsigned char* buf, unsigned int i);
int unpacki32(unsigned char* buf);
unsigned int unpacku32(unsigned char* buf);

void packi64(unsigned char* buf, unsigned long long int i);
long long int unpacki64(unsigned char* buf);
unsigned long long int unpacku64(unsigned char* buf);


#endif // __MY_LIBRARY_H__