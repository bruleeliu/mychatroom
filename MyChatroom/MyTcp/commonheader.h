#pragma once
#ifndef __COMMON_HEADER_H__
#define __COMMON_HEADER_H__

//#define _WIN32
#ifdef __linux__
#    undef _WIN32
#else
#    define _WIN32
#endif

#include <iostream>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h> // define uintN_t
#include <inttypes.h> // define PRIx macros
#include <string>
#include <cstring>
#include <signal.h>
#include <ctype.h>
#include <cctype>
#include <vector>

using namespace std;

#ifdef _WIN32
#pragma comment(lib, "Ws2_32.lib")
#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#define MyCloseSocket(x) closesocket(x)
#endif

#ifdef __linux__
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#include <sys/time.h>
#define MyCloseSocket(x) close(x)
#endif

#include "mylib.h"

#endif // __COMMON_HEADER_H__
