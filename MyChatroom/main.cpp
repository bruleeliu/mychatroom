#include "MyChatroom.h"
#include <QtWidgets/QApplication>
#include <QDebug>
#include <QWidget>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

int CheckConfigFile(MyChatroom* myChar)
{
	qDebug("CheckConfigFile()...");
	/* read config file */
	QFile file("config.mc");

	if (!file.open(QIODevice::ReadOnly))
	{
		qDebug() << "  error opening file 'config.mc' (error no is" << file.error() << ")";
		return 1;
	}

	QTextStream instream(&file);
	QString ip = instream.readLine();
	qDebug() << "  line 1: server ip = " << ip;
	QString port = instream.readLine();
	qDebug() << "  line 2: server port = " << port;
	//QString line = instream.readLine();
	//qDebug() << "  line 3: " << line;
	myChar->SetServerLocale(ip, port);
	file.close();

	return 0;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MyChatroom w;

	if (CheckConfigFile(&w) != 0)
	{
		QString msg = "\"config.mc\" not found!!";
		QMessageBox::critical(static_cast<QWidget*>(&w),
			w.windowTitle(),
			msg);
		a.exit(1);
		return 1;
	}

	w.show();

	return a.exec();
}
