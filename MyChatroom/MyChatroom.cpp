#include "MyChatroom.h"

void AssignUserName(char* inName, char* instr, int* len)
{
	*len = strlen(inName);// +1;
	if (*len >= MAXUSERNAMELENGTH)
		*len = MAXUSERNAMELENGTH;
	memset(instr, 0, MAXUSERNAMELENGTH);
	memcpy(instr, inName, *len/* - 1*/);
	//outMsgPack->name[outMsgPack->nameLen - 1] = '\0';
}

MyChatroom::MyChatroom(QWidget *parent)
	: QMainWindow(parent)
{
	qDebug("MyChatroom::MyChatroom()...");
	ui.setupUi(this);

	/* Initialize form UI */
	UiDialogInit();


	qDebug("  Do UI objects connect");
	/* connect button "Connect" */
	connect(ui.pushButton_connect, SIGNAL(clicked(bool)), this, SLOT(OnButtonConnectClicked()));
	connect(ui.pushButton_enter, SIGNAL(clicked(bool)), this, SLOT(OnButtonEnterClicked()));

	qDebug("  Do UI objects installEventFilter");
	/* install event to text "chat input" */
	ui.textEdit_chatinput->installEventFilter(this);

}

void MyChatroom::UiDialogInit()
{
	//QFont font;

	qDebug("UiDialogInit()...");

	/* initialize variables */
	isConnect = ConnectStatus::DISCONNECT; // default is in unconnect

	ui.textEdit_chatinput->setDisabled(true);
	ui.pushButton_enter->setDisabled(true);


	timerFuncSelect = 0;
	monitorTimer = new QTimer(this);
	connect(monitorTimer, SIGNAL(timeout()), this, SLOT(timerFunction()));
	monitorTimer->start(1000* TIMEOUT);

	myThread.isTerminate = false;
	myThread.isThreadCreate = false;

	///* modify 'user name' text font size */
	//font = ui.label_username->font();
	//font.setPointSize(12);
	//ui.label_username->setFont(font);

	///* modify 'status' label font size */
	//font = ui.label_staus->font();
	//font.setPointSize(12);
	//ui.label_staus->setFont(font);

	///* modify 'login' button font size */
	//font = ui.pushButton_connect->font();
	//font.setPointSize(12);
	//ui.pushButton_connect->setFont(font);
}

void MyChatroom::GetMsgFromServer()
{
	qDebug("MyChatroom::GetMsgFromServer()...");

	int status;
	struct timeval tv;
	UserJoinPackage usrJoinPack;
	MsgPackage msgPack;
	std::vector<SocketFdInfo> activityFdList;
	int sockFd;
	int value;
	QString str = "";
	bool isServerShutdown = false;

	while (1) 
	{
		qDebug("  >>this thread (id=%d), terminate=%d<<", std::this_thread::get_id(), myThread.isTerminate);
		if (myThread.isTerminate == true)
			break;
#if 0
		/* for testing */
		std::this_thread::sleep_for(std::chrono::seconds(1));
		qDebug("  >>print it (id=%d)", std::this_thread::get_id());
		//std::this_thread::yield();
#else
		//qDebug("  >>this thread (id=%d), terminate=%d<<", std::this_thread::get_id(), myThread.isTerminate);
		tv.tv_sec = TIMEOUT;
		tv.tv_usec = 0;
		status = myTcp.SelectWaitingfor(&tv);
		if (status > 0)
		{
			activityFdList.clear();
			if (myTcp.GetActivitySockfdClient(&activityFdList) != -1)
			{
				for (int list = 0; list < (int)activityFdList.size(); list++)
				{
					sockFd = activityFdList[list].sockFd; // sockfd value
					value = myTcp.GetPackType(NULL);
					if (value == 0)
					{
						/* server close...close client */
						qDebug("  server is shutdown...");
						isServerShutdown = true;
						timerFuncSelect = 21;
						break;
					}
					else if (value > 0)
					{
						if (myTcp.IsPackTypeInDefine(value) == false)
							continue;
						if (value == BROADCAST_USRJOIN)
						{
							qDebug("  receive a BROADCAST_USRJOIN message:");
							/* receive broadcast message */
							myTcp.ReceivePack(NULL, BROADCAST_USRJOIN, (void*)&usrJoinPack, &value, false);

							/* lock UI update */
							qDebug("    do LockUpdateUiFlag()");
							myThread.LockUpdateUiFlag();

							/* update UI */
							str = "[Server Notify]: " + QString(usrJoinPack.msg);
							qDebug("    message will be => '%s'\n", str);
							ui.textBrowser_msgout->append(str);

							/* unlock UI update */
							qDebug("    do UnlockUpdateUiFlag()");
							myThread.UnlockUpdateUiFlag();
						}
						if (value == COMMONMESSAGEPACK)
						{
							qDebug("  receive a COMMONMESSAGEPACK message:");
							/* receive common message */
							myTcp.ReceivePack(NULL, COMMONMESSAGEPACK, (void*)&msgPack, &value, false);

							/* lock UI update */
							qDebug("    do LockUpdateUiFlag()");
							myThread.LockUpdateUiFlag();

							/* update UI */
							str = QString(msgPack.name) + " said: \n  " + QString(msgPack.msg);
							qDebug("    message will be => '%s'\n", str);
							ui.textBrowser_msgout->append(str);

							/* unlock UI update */
							qDebug("    do UnlockUpdateUiFlag()");
							myThread.UnlockUpdateUiFlag();
						}
					}
				}
				if (isServerShutdown)
					break;
			}
		}
#endif
	}


	qDebug("  end of MyChatroom::GetMsgFromServer()");
}

void MyChatroom::AddInputChatMsgToTextOut()
{
	/// <summary>
	/// qDebug print QString str with %s:
	///    str should be transfered as below two methods,
	///       1. qPrintable(str)
	///       2. str.toStdString().data()
	/// </summary>
	qDebug("MyChatroom::AddInputChatMsgToTextOut()...");
	QString qUsername;
	QString qInputChat;
	QString qNewStr;
	MsgPackage msgPack;
	int value = 0;

	/* get input text */
	qUsername = ui.lineEdit_username->text();
	qInputChat = ui.textEdit_chatinput->toPlainText();
	value = strlen(qInputChat.toStdString().c_str());
	qDebug(" new input line (%d bytes) => %s", value, qInputChat.toStdString().data());
	qDebug("   user name => %s", qPrintable(qUsername));

	/* send message to server */
	if (isConnect == ConnectStatus::CONNECTED)
	{
		AssignUserName((char*)qUsername.toStdString().c_str(), msgPack.name, (int*)&msgPack.nameLen);
		msgPack.msgLen = value;
		if (msgPack.msgLen > MAXMSGLENGTH)
			msgPack.msgLen = MAXMSGLENGTH;
		memset(&msgPack.msg, 0, MAXMSGLENGTH);
		memcpy(&msgPack.msg, qInputChat.toStdString().c_str(), msgPack.msgLen);
		myTcp.SendPack(NULL, COMMONMESSAGEPACK, &msgPack, &value, false);
	}

	/* lock receive state */
	myThread.LockUpdateUiFlag();

	/* display message */
	qNewStr = qUsername + " said: \n  " + qInputChat;
	qDebug("   user name => %s", qPrintable(qNewStr));

	/* unlock receive state */
	myThread.UnlockUpdateUiFlag();

	/* clear input text */
	ui.textBrowser_msgout->append(qNewStr);
	ui.textEdit_chatinput->clear();
}

void MyChatroom::SetServerLocale(QString ip, QString port)
{
	ServerIP = ip;
	ServerPort = port;
}

void MyChatroom::ThreadStart()
{
	qDebug("MyChatroom::ThreadStart()...");
	//if (myThread.ptr != NULL)
	//{
	//	qDebug("  thread is not NULL (0x%x)",myThread.ptr);
	//	ThreadStop();
	//}

	myThread.ptr = new std::thread(&MyChatroom::GetMsgFromServer, this);

	myThread.id = myThread.ptr->get_id();
	myThread.handle = myThread.ptr->native_handle();
	qDebug("  thread ID: %d, handle: %d", myThread.id, myThread.handle);
	myThread.isTerminate = false;
	myThread.isThreadCreate = true;
	myThread.ClearUpdateUiFlag();

	std::this_thread::sleep_for(std::chrono::seconds(1));

	//qDebug("  before detach() or join(), thread joinable? %d", myThread.ptr->joinable());
	//myThread.ptr->detach();
	//pThread->join();
	//qDebug("  after detach() or join(), thread joinable? %d", myThread.ptr->joinable());
}

void MyChatroom::ThreadStop()
{
	qDebug("MyChatroom::ThreadStop()...");
	//std::call_once(mOnceFlag, [this] ()
		{
			qDebug("  called one");
			if (myThread.isThreadCreate)
			{
				qDebug("    myThread.ptr is not NULL (id:%d, handle:%d, joinable=%d)", myThread.id, myThread.handle, myThread.ptr->joinable());

				myThread.isTerminate = true;
				if (myThread.ptr->joinable())
					myThread.ptr->join();

				myThread.isThreadCreate = false;
				delete myThread.ptr;
				//myThread.ptr = NULL;
			}
		}
	//);
}
//============================================================
void MyChatroom::timerFunction()
{
	qDebug("MyChatroom::timerFunction()...");

	if (timerFuncSelect == 21)
	{
		timerFuncSelect = 0;
		/* to do disconnect */
		qDebug("  doing disconnect process");

		ui.label_staus->setText(tr("Disconnecting..."));
		QWidget::repaint();
		ui.textBrowser_msgout->append(tr("[Server is shutdown...]"));

		ThreadStop();
		myTcp.Close();

		isConnect = ConnectStatus::DISCONNECT;

		/* update UI display */
		ui.lineEdit_username->setDisabled(false); // unlock 'user name' text
		ui.textEdit_chatinput->setDisabled(true);
		ui.pushButton_enter->setDisabled(true);
		ui.pushButton_connect->setText("Connect");
		ui.label_staus->setText(tr("Not connect"));
		qDebug("  disconnect process done!");
	}
}

void MyChatroom::OnButtonConnectClicked()
{
	qDebug("MyChatroom::OnButtonConnectClicked()...");
	int status;

	if (isConnect == ConnectStatus::CONNECTED)
	{
		/* to do disconnect */
		qDebug("  doing disconnect process");

		ui.label_staus->setText(tr("Disconnecting..."));
		QWidget::repaint();

		ThreadStop();
		myTcp.Close();

		isConnect = ConnectStatus::DISCONNECT;

		/* update UI display */
		ui.lineEdit_username->setDisabled(false); // unlock 'user name' text
		ui.textEdit_chatinput->setDisabled(true);
		ui.pushButton_enter->setDisabled(true);
		ui.pushButton_connect->setText("Connect");
		ui.label_staus->setText(tr("Not connect"));
		qDebug("  disconnect process done!");
	}
	else if (isConnect == ConnectStatus::DISCONNECT)
	{
		/* to do connect */
		qDebug("  doing connect process");
		std::vector<SocketFdInfo> activityFdList;
		UserJoinPackage usrJoinPack;


		ui.label_staus->setText(tr("Connecting..."));
		QWidget::repaint();

		/* check user name */
		QString userName = ui.lineEdit_username->text();
		if (QString::compare(userName, "", Qt::CaseInsensitive) == 0)
		{
			QMessageBox::warning(this, tr("Warning"), tr("Please input user name!"));
			qDebug("    no user name be inputted");
			ui.label_staus->setText(tr("Connect failed"));
			return;
		}
		qDebug("    get user name is \"%s\"", qPrintable(userName));

		/* try to connect to server */
		status = myTcp.SetupClient(
			ServerIP.toStdString().c_str(),
			ServerPort.toStdString().c_str());
		if (status != 0)
		{
			qDebug("    connect to server failed");
			ui.label_staus->setText(tr("Connect failed"));
			return;
		}
		qDebug("    connect to server successfully");

		isConnect = ConnectStatus::CONNECTED; // connect to server successfully
		
		ThreadStart();
		
		/* send client info to server */
		int value = 0;
		AssignUserName((char*)userName.toStdString().c_str(),
			usrJoinPack.msg, (int*)&usrJoinPack.msgLen);
		myTcp.SendPack(NULL, BROADCAST_USRJOIN, (void*)&usrJoinPack, &value, false); // send clinet name to server
		qDebug("    sent BROADCAST_USRJOIN to server");

		/* do some other settings */
		//myTcp.AddToMainFdList(STDIN);

		/* update UI display */
		ui.lineEdit_username->setDisabled(true); // lock 'user name' text
		ui.textEdit_chatinput->setDisabled(false);
		ui.pushButton_enter->setDisabled(false);
		ui.pushButton_connect->setText("Disconnect");
		ui.label_staus->setText(tr("Connected to server"));
		qDebug("  connect process done!");
	}
}

void MyChatroom::OnButtonEnterClicked()
{
	qDebug("MyChatroom::OnButtonEnterClicked()...");
	AddInputChatMsgToTextOut();
}

//============================================================
bool MyChatroom::eventFilter(QObject* obj, QEvent* event)
{
	//qDebug("EventFilter()...");
	QKeyEvent* keyEvent;

	if (obj == ui.textEdit_chatinput && event->type() == QEvent::KeyPress)
	{
		//qDebug("  obj = %s", ui.textEdit_chatinput->objectName().toStdString());
		keyEvent = static_cast<QKeyEvent*>(event);
		if (keyEvent->key() == Qt::Key_Enter)
		{
			qDebug("  key(0x%x): enter pressed ", keyEvent->key());
			AddInputChatMsgToTextOut();
			return true;
		}
		else
			return false;
	}
	else 
	{
		//// standard event processing
		//return QObject::eventFilter(obj, event);
		return false;
	}
}

void MyChatroom::closeEvent(QCloseEvent* event)
{
	qDebug("MyChatroom::closeEvent()...");

	/* close process */
	if (monitorTimer->isActive())
		monitorTimer->stop();
	monitorTimer->~QTimer();
	ThreadStop();
	myTcp.Close();
}
//============================================================
void MyChatroom::TestFunc()
{
	int status;
	struct timeval tv;
	UserJoinPackage usrJoinPack;
	std::vector<SocketFdInfo> activityFdList;
	int sockFd;
	int value;


	tv.tv_sec = 60;
	tv.tv_usec = 0;
	status = myTcp.SelectWaitingfor(&tv);
	if (status > 0)
	{
		activityFdList.clear();
		if (myTcp.GetActivitySockfdClient(&activityFdList) != -1)
		{
			for (int list = 0; list < (int)activityFdList.size(); list++)
			{
				sockFd = activityFdList[list].sockFd; // sockfd value
				value = myTcp.GetPackType(NULL);
				if (value > 0)
				{
					if (myTcp.IsPackTypeInDefine(value) == false)
						continue;
					if (value == BROADCAST_USRJOIN)
					{
						/* receive broadcast message */
						myTcp.ReceivePack(NULL, BROADCAST_USRJOIN, (void*)&usrJoinPack, &value, false);
						InfoMsg("[Broadcast Message]: %s\n", usrJoinPack.msg);
						QString str = "[Server Notify]: " + QString(usrJoinPack.msg);
						ui.textBrowser_msgout->append(str);
					}
				}
			}
		}
	}
}