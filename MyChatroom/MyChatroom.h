#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MyChatroom.h"
#include <QDebug>
#include <QKeyEvent>
#include <QMessageBox>
#include "MyTcp/mytcp.h"
#include <thread>
#include <mutex>
#include <QWidget>
#include <QTimer>

#define TIMEOUT 1 // second

enum class ConnectStatus
{
	CONNECTED = 0,
	//CONNECTING = 1,
	DISCONNECT = 2,
	//CONNECT_SUCCESS = 10,
	//DISCONNECT_SUCCESS = 11,
	//CONNECT_FAILURE = 20,
	//DISCONNECT_FAILURE = 21,
	UNKNOWN = 999
};

struct MyThread
{
private:
	bool _isInUpdateDisplayUI;
public:
	std::thread* ptr;
	std::thread::id id;
	std::thread::native_handle_type handle;
	bool isThreadCreate;
	bool isTerminate;

	void ClearUpdateUiFlag() { _isInUpdateDisplayUI = false; }
	void LockUpdateUiFlag() {
		while (_isInUpdateDisplayUI) {}// not in receive state
		_isInUpdateDisplayUI = true;
	}
	void UnlockUpdateUiFlag() { _isInUpdateDisplayUI = false; }
	//bool IsInReceiveState() { return _isInReceiveData; }
	//void SetReceiveState(bool isReceive) { _isInReceiveData = isReceive; }
};

class MyChatroom : public QMainWindow
{
	Q_OBJECT

public:
	MyChatroom(QWidget *parent = Q_NULLPTR);
	void SetServerLocale(QString ip, QString port);

protected slots:
	void OnButtonConnectClicked();
	void OnButtonEnterClicked();
	void closeEvent(QCloseEvent* event);
	void timerFunction();

signals:
	//ex: void sendData(QString);

protected:
	bool eventFilter(QObject* obj, QEvent* event); // function name  cannot be changed!!
	
private:
	void UiDialogInit();
	void AddInputChatMsgToTextOut();
	void TestFunc();
	void GetMsgFromServer();
	void ThreadStart();
	void ThreadStop();


private:
	Ui::MyChatroomClass ui;
	ConnectStatus isConnect;
	QString ServerIP;
	QString ServerPort;
	QTimer* monitorTimer;
	int timerFuncSelect;
	MyBasicTcp myTcp; // for TCP socket
	MyThread myThread;
	std::once_flag mOnceFlag;
};
